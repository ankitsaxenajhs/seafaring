import React, {Component} from 'react';
import axios from 'axios';
import './NewBlog.css';
import Navbar from "../Navigation/Navbar";
class NewBlog extends Component {

    componentDidMount() {
        window.scrollTo(0,0);
    }


    state={
        name:"",
        image:"",
        description:""
    };

    nameChangedHandler = (event) => {
        this.setState({name: event.target.value});
    };

    imageChangedHandler = (event) => {
        this.setState({image: event.target.value});
    };

    descriptionChangedHandler = (event) => {
        this.setState({description: event.target.value});
    };

    submitBlog = (e) =>{
        e.preventDefault();
        console.log(this.state);
        axios.post("https://peaceful-wind-cave-44493.herokuapp.com/campgrounds",this.state)
            .then(response=>{
                console.log(response);
                if(response.data.message==='success'){
                    window.location="/bloglist";
                }
            }).catch(err=>{
                console.log(err);
        })
    };


    render() {

        let loader = (
            <div className="d-flex justify-content-center">
                <div className="spinner-grow text-primary" style={{width: "20rem", height: "20rem" ,marginTop:"11em"}} role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        );

        //if(window.localStorage.getItem("username")==='seafaring'){
            loader=(
                <div className="ownContainer">
                    <div className="ui main text container segment">
                        <div className="ui huge header">New Blog</div>
                        <form className="ui form">

                            <div className="field">
                                <label>Title</label>
                                <input type="text" value={this.state.name} name="name" placeholder="name" onChange={(event) => this.setState({name: event.target.value})}/>
                            </div>
                            <div className="field">
                                <label>Image URL</label>
                                <input type="text" name="image" value={this.state.image} placeholder="image url" onChange={(event) => this.setState({image: event.target.value})}/>
                            </div>
                            <div className="field">
                                <label>Description</label>
                                <textarea type="text" name="description" value={this.state.description} placeholder="description" onChange={(event) => this.setState({description: event.target.value})}></textarea>
                            </div>
                        </form>
                        <div className="ownButton">
                            <button className="ui violet inverted big button" onClick={this.submitBlog}>SUBMIT</button>
                        </div>
                    </div>
                </div>
            )

        return (
            <div>
                <Navbar/>
                <div className='mySection'>
                    {loader}
                </div>
            </div>
        );
    }
}

export default NewBlog;