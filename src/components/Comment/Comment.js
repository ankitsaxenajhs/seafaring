import React, {Component} from 'react';

class Comment extends Component {
    render() {
        console.log(this.props.comments);
        return (
            <div>
                {this.props.comments.map(comment=>{
                    let commentDate='';
                    let currentNewDate = new Date(comment.createdComment);
                    if(comment.createdComment){
                        commentDate=currentNewDate.toDateString();
                    }
                    return (<div className="row mb-4">

                        <div className="col-sm-2 col-12 mb-md-0 mb-3">
                            {/*<img src="https://mdbootstrap.com/img/Photos/Avatars/img (9).jpg" class="avatar rounded-circle z-depth-1-half"/>*/}
                        </div>

                        <div className="col-sm-10 col-12">
                            <a>
                                <h4 className="font-weight-bold">{comment.author}</h4>
                            </a>
                            <div className="mt-2">
                                <ul className="list-unstyled">
                                    <li className="comment-date"><i className="fas fa-clock"></i> {commentDate}</li>
                                </ul>
                            </div>
                            <p className="grey-text">{comment.text}</p>
                        </div>

                    </div>)
                })}

            </div>
        );
    }
}

export default Comment;