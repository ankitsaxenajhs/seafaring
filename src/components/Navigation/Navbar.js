import React, {Component} from 'react';
import axios from 'axios';
import {withRouter} from 'react-router';
import './Navbar.css';
class Navbar extends Component {

    constructor(props){
        super(props);
        this.state={
            username:'',
            password:'',
            rusername:'',
            rpassword:''
        }
    }

    signInHandler =() =>{
        console.log("in signInHandler");
        console.log(this.state);
        axios.post("https://peaceful-wind-cave-44493.herokuapp.com/login",{username:this.state.username,password:this.state.password})
            .then(response=>{
                console.log(response);
                if(response.data.message==='success'){
                    window.localStorage.setItem("message","success");
                    window.localStorage.setItem("username",response.data.username);
                    window.location.reload();
                }
            }).catch(err=>{
                console.log("not authorized")
        });

    };

    signOutHandler =()=>{
        console.log("in signOutHandler");
        // window.localStorage.setItem("message","unsuccess");
        // window.localStorage.setItem("username","");
        window.localStorage.clear();
        window.location.reload();
    };

    signUpHandler=()=>{
        axios.post("https://peaceful-wind-cave-44493.herokuapp.com/register",{username:this.state.rusername,password:this.state.rpassword})
            .then(response=>{
                console.log(response);
                if(response.data.registerMessage==='success'){
                    window.localStorage.setItem("message","success");
                    window.localStorage.setItem("username",response.data.username);
                    window.location.reload();
                }
            }).catch(err=>{
            console.log("not authorized")
        });
    };

    deleteBlogHandler = ()=>{
        axios.delete("https://peaceful-wind-cave-44493.herokuapp.com/campgrounds/"+this.props.match.params._id)
            .then(response=>{
                if(response.data.deleteMessage==='success'){
                    window.location="/bloglist";
                }
                else{
                    window.location.reload();
                }
            })
    };

    render() {
        // window.localStorage.setItem("message","success");
        let message=(window.localStorage.getItem("message"));
        let username=(window.localStorage.getItem("username"));
        console.log(message);
        let profile='';
        let userDetail='';
        let signInSuccess='';
        let extraNav = '';
        let newNav = '';
        console.log(this.props);
        if(message!=='success'){
            // profile=(
            //     <div onClick={this.signInHandler}>
            //         <a className="dropdown-item" href="#">Sign In</a>
            //     </div>
            //
            // );
            userDetail=(<span>NoOne</span>);
            signInSuccess = (
                <>
                    <li className="nav-item">
                        <a href="" data-toggle="modal" className="nav-link navLink"
                           data-target="#modalLRForm">SignIn/SignUp</a>
                    </li>
                </>)

        }
        else if(message==='success'){
            // profile=(
            //     <div>
            //         <a className="dropdown-item" href="#">My account</a>
            //         <a onClick={this.signOutHandler} className="dropdown-item" href="#">Log out</a>
            //     </div>
            // );
            userDetail=(<span>{window.localStorage.getItem("username")}</span>);
            signInSuccess = (
                <>
                    <li className="nav-item active">
                        <a className="nav-link" href="#">
                            Sign in as {userDetail}
                            <span className="sr-only">(current)</span>
                        </a>
                    </li>
                    <li className="nav-item active">
                        <a className="nav-link" href="#" onClick={this.signOutHandler}>
                            LogOut
                        </a>
                    </li>
                </>
            )
        }
        //if(message==='success'&&username==='seafaring'&&this.props.match.params._id){
        if(message==='success'&&this.props.match.params._id){
            extraNav=(
                <>
                    <li className="nav-item">
                        <a className="nav-link navLink" href={`/bloglist/details/${this.props.match.params._id}/edit`}>Edit</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link navLink" onClick={this.deleteBlogHandler}>Delete</a>
                    </li>
                </>
            );
        }
        //if(message==='success'&&username==='seafaring'){
        if(message==='success'){
            newNav = (
                <>
                    <li className="nav-item">
                        <a className="nav-link navLink" href="/bloglist/new">New</a>
                    </li>
                </>
            )
        }
        return (
            <div>
                <div className="modal fade" id="modalLRForm" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div className="modal-dialog cascading-modal" role="document">

                        <div className="modal-content">


                            <div className="modal-c-tabs">


                                <ul className="nav nav-tabs md-tabs tabs-2 light-blue darken-3" role="tablist">
                                    <li className="nav-item">
                                        <a className="nav-link active" id="navLink" data-toggle="tab" href="#panel7" role="tab"><i
                                            className="fas fa-user mr-1"></i>
                                            Login</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" id="navLink" data-toggle="tab" href="#panel8" role="tab"><i
                                            className="fas fa-user-plus mr-1"></i>
                                            Register</a>
                                    </li>
                                </ul>


                                <div className="tab-content">

                                    <div className="tab-pane fade in show active" id="panel7" role="tabpanel">


                                        <div className="modal-body mb-1">
                                            <div className="md-form form-sm mb-5">
                                                <i className="fas fa-envelope prefix"></i>
                                                <input
                                                    type="email"
                                                    id="modalLRInput10"
                                                    className="form-control form-control-sm validate"
                                                    placeholder="Your Username"
                                                    onChange={(event) => this.setState({username: event.target.value})}
                                                />
                                                {/*<label data-error="wrong" data-success="right"*/}
                                                {/*       htmlFor="modalLRInput10">Your email</label>*/}
                                            </div>

                                            <div className="md-form form-sm mb-4">
                                                <i className="fas fa-lock prefix"></i>
                                                <input
                                                    type="password"
                                                    id="modalLRInput11"
                                                    className="form-control form-control-sm validate"
                                                    placeholder="Your Password"
                                                    onChange={(event) => this.setState({password: event.target.value})}
                                                />
                                                {/*<label data-error="wrong" data-success="right"*/}
                                                {/*       htmlFor="modalLRInput11">Your password</label>*/}
                                            </div>
                                            <div className="text-center mt-2">
                                                <button className="btn btn-info" onClick={this.signInHandler}>Log in <i
                                                    className="fas fa-sign-in ml-1"></i></button>
                                            </div>
                                        </div>

                                        <div className="modal-footer">
                                            <div className="options text-center text-md-right mt-1">
                                                <p>Not a member? <a href="#" className="blue-text">Sign Up</a></p>
                                                <p>Forgot <a href="#" className="blue-text">Password?</a></p>
                                            </div>
                                            <button type="button" className="btn btn-outline-info waves-effect ml-auto"
                                                    data-dismiss="modal">Close
                                            </button>
                                        </div>

                                    </div>

                                    <div className="tab-pane fade" id="panel8" role="tabpanel">


                                        <div className="modal-body">
                                            <div className="md-form form-sm mb-5">
                                                <i className="fas fa-envelope prefix"></i>
                                                <input
                                                    type="email"
                                                    id="modalLRInput12"
                                                    className="form-control form-control-sm validate"
                                                    placeholder="Your Email"
                                                    onChange={(event) => this.setState({rusername: event.target.value})}
                                                />
                                                {/*<label data-error="wrong" data-success="right"*/}
                                                {/*       htmlFor="modalLRInput12">Your email</label>*/}
                                            </div>

                                            <div className="md-form form-sm mb-5">
                                                <i className="fas fa-lock prefix"></i>
                                                <input
                                                    type="password"
                                                    id="modalLRInput13"
                                                    className="form-control form-control-sm validate"
                                                    placeholder="Your Password"
                                                    onChange={(event) => this.setState({rpassword: event.target.value})}
                                                />

                                                {/*<label data-error="wrong" data-success="right"*/}
                                                {/*       htmlFor="modalLRInput13">Your password</label>*/}
                                            </div>

                                            {/*<div className="md-form form-sm mb-4">*/}
                                            {/*    <i className="fas fa-lock prefix"></i>*/}
                                            {/*    <input type="password" id="modalLRInput14"*/}
                                            {/*           className="form-control form-control-sm validate"/>*/}
                                            {/*    <label data-error="wrong" data-success="right"*/}
                                            {/*           htmlFor="modalLRInput14">Repeat password</label>*/}
                                            {/*</div>*/}

                                            <div className="text-center form-sm mt-2">
                                                <button className="btn btn-info" onClick={this.signUpHandler}>Sign up <i
                                                    className="fas fa-sign-in ml-1"></i></button>
                                            </div>

                                        </div>

                                        <div className="modal-footer">
                                            <div className="options text-right">
                                                <p className="pt-1">Already have an account? <a href="#"
                                                                                                className="blue-text">Log
                                                    In</a></p>
                                            </div>
                                            <button type="button" className="btn btn-outline-info waves-effect ml-auto"
                                                    data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <nav class="mb-1 fixed-top navbar navbar-expand-lg navbar-dark indigo">
                    <img style={{height:'40px'}} className='navbar-brand' src="https://lh3.googleusercontent.com/njEydfkmAjjqV7NUoLBZuNjm-ODebOlrISmiwttgwvjG9CYWoz1ZI0LADC1uWpeMg7BPr1oPLTlSWUZ6cY63WTh2L2Svs7cq59E8oCXB-fFmWJseQS3-nCxNPlICMnGGKYUSiogrRBIY_gn_yHkiAN41fQWjpvNrrep0KUC2HHVbjbqzfhJMzNdV4EXVrgduiFnt-xr5ugMoPp2RZkDsYmtM94Eb77e0EdK9UUhkTb8SL1vq3TdF2FV46aDN3KMHBzL6rWaIXLDWwTYGe3UFJ_BoZ01GOtFkyNDHTaR_xpM5c5R78od0VXWjh9y4T2ghrZciUbTAdVvi4sqX-UYfUQ0wbb7oQ18RfCSIWRxP_Kgw-WCKEsl4ZYcYUQWJ0QOkfeUkdY7p4TtAMwl5qUBYO9F3r_9Tz_58WA1Boe4mi-yX_FHNtqoSBLmnwQzHxjKsHMSndzeZuoGJBZE-t199yH_LUnSo86pZI_EtmcRQM1N1nAjtCMUPE2ESjj0J8beZKS4KJQOX-KMBvQCWXQ-6-rdJjrX27RiALTQyg05nP4POK7DbphNOg-fnsi0MTqASulYqDHLWpYVxhsEbQ_k0TtpCUGd2oKAxrTED9GYh2_rzi7og17S7RBaE05_g5j2B0gTIMc7BfrsopFkSDuZtiRygZOEB78sj0TRSWaYWFSPSemOgt3SdQg1OLc887nXLN2LJ6U-CxjHTkLX2KOEwzZhXag=w633-h625-no"/>

                    <a class="navbar-brand" style={{justifyContent:'center'}} href="/">Seafaring World</a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4"
                            aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <a className="nav-link navLink" href="/bloglist">Home</a>
                            </li>
                            {newNav}
                            <li className="nav-item">
                                <a className="nav-link navLink" href="/contactus">Contact Us</a>
                            </li>
                            {extraNav}
                            {signInSuccess}
                        </ul>


                    </div>
                </nav>

            </div>
        );
    }
}

export default withRouter(Navbar);