import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';

class Landing extends Component {

    redirect = () =>{
        return <Redirect to="/bloglist"/>
    };

    render() {

        return (
            <div>
                {this.redirect()}
                <section class="text-center my-5">


                    <h2 class="h1-responsive font-weight-bold my-5">Recent posts</h2>

                    <p class="dark-grey-text w-responsive mx-auto mb-5">Duis aute irure dolor in reprehenderit in voluptate velit
                        esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
                        qui officia deserunt mollit anim id est laborum.</p>


                    <div class="row">


                        <div class="col-lg-4 col-md-6 mb-md-0 mb-4">


                            <div class="view overlay rounded z-depth-2 mb-4">
                                <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg" alt="Sample image"/>
                                    <a>
                                        <div class="mask rgba-white-slight"></div>
                                    </a>
                            </div>


                            <a href="#!" class="deep-orange-text">
                                <h6 class="font-weight-bold mb-3"><i class="fas fa-graduation-cap pr-2"></i>Education</h6>
                            </a>

                            <h4 class="font-weight-bold mb-3"><strong>Title of the news</strong></h4>

                            <p>by <a class="font-weight-bold">Billy Forester</a>, 13/07/2018</p>

                            <p class="dark-grey-text">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                                voluptatum deleniti atque corrupti quos dolores.</p>

                            <a class="btn btn-deep-orange btn-rounded btn-md">Read more</a>

                        </div>

                        <div className="col-lg-4 col-md-6 mb-md-0 mb-4">


                            <div className="view overlay rounded z-depth-2 mb-4">
                                <img className="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg"
                                     alt="Sample image"/>
                                <a>
                                    <div className="mask rgba-white-slight"></div>
                                </a>
                            </div>


                            <a href="#!" className="deep-orange-text">
                                <h6 className="font-weight-bold mb-3"><i className="fas fa-graduation-cap pr-2"></i>Education
                                </h6>
                            </a>

                            <h4 className="font-weight-bold mb-3"><strong>Title of the news</strong></h4>

                            <p>by <a className="font-weight-bold">Billy Forester</a>, 13/07/2018</p>

                            <p className="dark-grey-text">At vero eos et accusamus et iusto odio dignissimos ducimus qui
                                blanditiis
                                voluptatum deleniti atque corrupti quos dolores.</p>

                            <a className="btn btn-deep-orange btn-rounded btn-md">Read more</a>

                        </div>

                        <div className="col-lg-4 col-md-6 mb-md-0 mb-4">


                            <div className="view overlay rounded z-depth-2 mb-4">
                                <img className="img-fluid" src="https://mdbootstrap.com/img/Photos/Others/images/43.jpg"
                                     alt="Sample image"/>
                                <a>
                                    <div className="mask rgba-white-slight"></div>
                                </a>
                            </div>


                            <a href="#!" className="deep-orange-text">
                                <h6 className="font-weight-bold mb-3"><i className="fas fa-graduation-cap pr-2"></i>Education
                                </h6>
                            </a>

                            <h4 className="font-weight-bold mb-3"><strong>Title of the news</strong></h4>

                            <p>by <a className="font-weight-bold">Billy Forester</a>, 13/07/2018</p>

                            <p className="dark-grey-text">At vero eos et accusamus et iusto odio dignissimos ducimus qui
                                blanditiis
                                voluptatum deleniti atque corrupti quos dolores.</p>

                            <a className="btn btn-deep-orange btn-rounded btn-md">Read more</a>

                        </div>


                    </div>


                </section>

            </div>
        );
    }
}

export default Landing;