import React, {Component} from 'react';
import {Link} from "react-router-dom";
import axios from 'axios';
import './BlogDetail.css';
import Comment from "../Comment/Comment";
import Navbar from "../Navigation/Navbar";

class BlogDetail extends Component {

    constructor(props){
        super(props);
        this.state = {
            blogDetail:[],
            text:'',
            author:'',
            loading:true
        };
        this.commentSubmitHandler=this.commentSubmitHandler.bind(this);
    }
    componentDidMount() {

        window.scrollTo(0,0);

        axios.get("https://peaceful-wind-cave-44493.herokuapp.com/campgrounds/"+this.props.match.params._id,{headers: {'Content-Type':'application/json','Access-Control-Allow-Origin': '*'}})
            .then(response=>{
                console.log(response);
                if(response.data!==''){
                    this.setState({blogDetail:response.data,comments:response.data.comments,loading:false});
                }
                console.log(this.state.blogDetail.comments.length);
            }).catch(err=>{
                console.log("error");
                console.log(err);
        })

    }

    renderComments() {
        if(!this.state.blogDetail.comments){
            return null;
        }
        return (<Comment comments={this.state.blogDetail.comments} key={this.state.blogDetail._id}/>);
    }

    commentSubmitHandler(){
        console.log("dsshbvhsdbv");
        console.log(this.state);
        const comment = {text:this.state.text,author:this.state.author};
        console.log(comment);
        axios.post("https://peaceful-wind-cave-44493.herokuapp.com/campgrounds/"+this.props.match.params._id+"/comments",comment)
            .then(response=>{
                console.log("yes comment submitted");
                window.location.reload();
            }).catch(err=>{
                console.log("not submitted");
        });
        console.log("hsbvhskbhbcsdc ");
    }

    deleteBlogHandler = ()=>{
        axios.delete("https://peaceful-wind-cave-44493.herokuapp.com/campgrounds/"+this.props.match.params._id)
            .then(response=>{
                if(response.data.deleteMessage==='success'){
                    window.location="/bloglist";
                }
                else{
                    window.location.reload();
                }
            })
    };

    render() {
        const {description,image,created,name,comments} = this.state.blogDetail;
        console.log(this.state.text);
        let commentLength=0;
        if(comments){
            commentLength=comments.length;
        }

        let currentDate = new Date(created);
        var newDate="";
        var newDescription="";
        if(created&&description){
            newDate = currentDate.toDateString();
            newDescription = description.substring(0,100);
        }
        let replyForm = (<p className="text-center">You Must Logged In to Add a Comment</p>);
        if(window.localStorage.getItem("message")==='success'){
            replyForm=(
                <div className="row text-center text-md-left">

                    <div className="col-sm-2 col-12 mb-md-0 mt-4">
                        {/*<img src="https://mdbootstrap.com/img/Photos/Avatars/img (32).jpg" alt="Sample avatar image" class="img-fluid avatar rounded-circle z-depth-2"/>*/}
                    </div>
                    <div className="col-sm-10 col-12">
                        <div className="md-form">
                            {/*<form method="POST" action={`https://imdb-rating-ankitsaxenajhs.c9users.io/campgrounds/${this.props.match.params._id}/comments`}>*/}
                                <textarea
                                    type="text"
                                    id="form-mess"
                                    className="md-textarea form-control"
                                    placeholder="Your Message"
                                    rows="3"
                                    onChange={(event) => this.setState({text: event.target.value})}
                                    value={this.state.text}
                                    name="comment[text]">
                                </textarea>
                                <input
                                    placeholder="Your Name"
                                    className="md-textarea form-control"
                                    type="text"
                                    value={this.state.author}
                                    onChange={(event) => this.setState({author: event.target.value})}
                                    name="comment[author]"
                                />
                                <div className="col-md-12 text-center mt-4"
                                     onClick={this.commentSubmitHandler}>
                                    <button className="btn btn-pink btn-rounded" style={{float:"left"}} >Submit</button>
                                </div>
                            {/*</form>*/}

                        </div>

                    </div>

                </div>
            )

        }

        let loader = (
            <div className="d-flex justify-content-center">
                <div className="spinner-grow text-primary" style={{width: "20rem", height: "20rem" ,marginTop:"11em"}} role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        );

        let editDelete = '';
        if(window.localStorage.getItem("username")==='seafaring'){
            editDelete=(
                <>
                    <Link to={`/bloglist/details/${this.props.match.params._id}/edit`}>
                        <button className="ui purple button">Edit</button>
                    </Link>
                    <button onClick={this.deleteBlogHandler} className="ui red button">Delete</button>
                </>
            )
        }

        if(this.state.loading===false){
            loader=(
                <main>

                    <div className="container">


                        <section className="section mt-5 pb-3 wow fadeIn">


                            <div className="row">
                                <div className="col-md-12">

                                    <div className="card card-cascade wider reverse">


                                        <div className="view view-cascade overlay">
                                            <img className="card-img-top"
                                                 src={image}
                                                 alt="Card image cap"/>
                                            <a href="#!">
                                                <div className="mask rgba-white-slight"></div>
                                            </a>
                                        </div>


                                        <div className="card-body card-body-cascade text-center">
                                            <h2><a><strong>{name}</strong></a></h2>
                                            <p>Written by <a>Meet Mehta</a>, {newDate}</p>

                                        </div>

                                    </div>


                                    <div className="excerpt mt-5 wow fadeIn" data-wow-delay="0.3s">

                                        <blockquote className="blockquote mt-4 mb-4">
                                            <p className="mb-0"><em><strong>{description}</strong></em></p>
                                            <footer className="blockquote-footer">Seafaring World<cite
                                                title="Source Title"> - Meet Mehta</cite></footer>
                                        </blockquote>


                                        <div className="mt-4 d-flex justify-content-end">
                                            <span className="badge pink">Travel</span>
                                            <span className="badge badge-primary mx-1">Adventure</span>
                                            <span className="badge grey mr-1">Photography</span>
                                            <span className="badge badge-info">Education</span>
                                        </div>

                                    </div>
                                </div>
                                {editDelete}
                            </div>


                        </section>


                    <section>

                        <div className="jumbotron p-5 text-center text-md-left author-box wow fadeIn"
                             data-wow-delay="0.3s">

                            <h4 className="h3-responsive text-center font-weight-bold dark-grey-text">About author</h4>
                            <hr/>
                            <div className="row">

                                <div className="col-12 col-md-2 mb-md-0 mb-4">
                                    {/*<img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(32).jpg" class="img-fluid rounded-circle z-depth-2"/>*/}
                                </div>

                                <div className="col-12 col-md-10">
                                    <h5 className="font-weight-bold dark-grey-text mb-3">
                                        <strong>Meet Mehta</strong>
                                    </h5>
                                    <div className="personal-sm pb-3">
                                        <a className="fb-ic pr-2"><i className="fab fa-facebook-f mr-2"> </i></a>
                                        <a className="tw-ic pr-2"><i className="fab fa-twitter mr-2"> </i></a>
                                        <a className="gplus-ic pr-2"><i className="fab fa-google-plus-g mr-2"> </i></a>
                                        <a className="li-ic pr-2"><i className="fab fa-linkedin-in mr-2"> </i></a>
                                    </div>
                                    <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo
                                        minus.</p>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint esse nulla quia
                                        quam veniam commodi
                                        dicta, iusto inventore. Voluptatum pariatur eveniet ea, officiis vitae
                                        praesentium beatae
                                        quas libero, esse facere.
                                    </p>
                                </div>
                            </div>
                        </div>

                    </section>


                    <section className="mb-4 pt-5 wow fadeIn" data-wow-delay="0.3s">


                        <div className="comments-list text-center text-md-left mb-5">
                            <div className="text-center mb-4">
                                <h3 className="font-weight-bold pt-3 mb-5">Comments <span
                                    className="badge pink">{commentLength}</span></h3>
                            </div>

                            {this.renderComments()}

                        </div>

                    </section>

                    <section className="pb-5 mt-5 wow fadeIn" data-wow-delay="0.3s">


                        <div className="reply-form">
                            <h3 className="text-center my-5 h3 pt-3">Leave a reply </h3>
                            {replyForm}


                        </div>


                    </section>
                    </div>
                </main>
            )
        }

        return (
            <div>
                <Navbar/>
                <div className='mySection'>
                    {loader}
                </div>
            </div>
        );
    }
}

export default BlogDetail;