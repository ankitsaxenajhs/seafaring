import React, {Component} from 'react';
import {Link} from "react-router-dom";
import './Blog.css';

class Blog extends Component {



    render() {
        console.log(this.props.blog);
        const {_id,description,image,created,name} = this.props.blog;
        let currentDate = new Date(created);
        var newDate="";
        var newDescription="";
        if(created&&description){
            newDate = currentDate.toDateString();
            newDescription = description.substring(0,100);
        }


        return (


                <div className="col-lg-4 col-md-6 mb-md-0 mb-4" style={{marginTop:"50px"}}>


                    <div className="view overlay rounded z-depth-2 mb-4">
                        <img className="img-fluid" src={image}
                             alt="Sample image"/>
                        <a>
                            <div className="mask rgba-white-slight"></div>
                        </a>
                    </div>


                    <a href="#!" className="blue-text">
                        <h6 className="font-weight-bold mb-3"><i className="fas fa-map pr-2"></i>Adventure
                        </h6>
                    </a>

                    <h4 className="font-weight-bold mb-3"><strong>{name}</strong></h4>

                    <p>by <a className="font-weight-bold">Meet Mehta</a>, {newDate}</p>

                    <p className="dark-grey-text">{newDescription}</p>

                    <Link to={`/bloglist/details/${_id}`}>
                        <button className="btn btn-info btn-rounded btn-md">Read more</button>
                        {/*<a className="btn btn-info btn-rounded btn-md" href={`/bloglist/details/${_id}`}>Read more</a>*/}
                    </Link>

                </div>

        );
    }
}

export default Blog;