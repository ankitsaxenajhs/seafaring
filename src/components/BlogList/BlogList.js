import React, {Component} from 'react';
import axios from 'axios';
import Blog from "./Blog";
import './Blog.css';
import Navbar from "../Navigation/Navbar";


class BlogList extends Component {

    state={
        blogList:[],
        loading:true
    };
    componentDidMount() {

        window.scrollTo(0,0);

        axios.get("https://peaceful-wind-cave-44493.herokuapp.com/campgrounds")
            .then(response=>{

                this.setState({blogList:response.data,loading:false});
                console.log(this.state.blogList);
            })
    }

    render() {

        let loader = (
            <div className="d-flex justify-content-center">
                <div className="spinner-grow text-primary" style={{width: "20rem", height: "20rem" ,marginTop:"2em"}} role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        );

        if(this.state.loading===false){
            loader=(
                <div className="row">
                    {this.state.blogList.map(blog=>{
                        return <Blog key={blog._id} blog={blog}/>
                    })}
                </div>
            )
        }


        return (

            <div className="container">
                <Navbar/>
                <section className="text-center my-5 mySection">

                    <h2 className="h1-responsive font-weight-bold my-5">Recent posts</h2>
                    {loader}

                </section>
            </div>
        );
    }
}

export default BlogList;