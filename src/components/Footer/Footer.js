import React, {Component} from 'react';
import {Link} from "react-router-dom";
import './Footer.css';

class Footer extends Component {

    render() {

        return (
            <div>
                <div className="modal fade" id="modalContactForm" tabIndex="-1" role="dialog"
                     aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header text-center">
                                <h4 className="modal-title w-100 font-weight-bold">Write to us</h4>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body mx-3">
                                <div className="md-form mb-5">
                                    <i className="fas fa-user prefix grey-text"></i>
                                    <input type="text" id="form34" className="form-control validate"/>
                                    <label data-error="wrong" data-success="right" htmlFor="form34">Your
                                        name</label>
                                </div>

                                <div className="md-form mb-5">
                                    <i className="fas fa-envelope prefix grey-text"></i>
                                    <input type="email" id="form29" className="form-control validate"/>
                                    <label data-error="wrong" data-success="right" htmlFor="form29">Your
                                        email</label>
                                </div>

                                <div className="md-form mb-5">
                                    <i className="fas fa-tag prefix grey-text"></i>
                                    <input type="text" id="form32" className="form-control validate"/>
                                    <label data-error="wrong" data-success="right"
                                           htmlFor="form32">Subject</label>
                                </div>

                                <div className="md-form">
                                    <i className="fas fa-pencil prefix grey-text"></i>
                                    <textarea type="text" id="form8" className="md-textarea form-control"
                                              rows="4"></textarea>
                                    <label data-error="wrong" data-success="right" htmlFor="form8">Your
                                        message</label>
                                </div>

                            </div>
                            <div className="modal-footer d-flex justify-content-center">
                                <button className="btn btn-unique">Send <i
                                    className="fas fa-paper-plane-o ml-1"></i></button>
                            </div>
                        </div>
                    </div>
                </div>

                <footer class="page-footer font-small special-color-dark myFooter">




                    <div class="container">


                        <div class="row text-center d-flex justify-content-center pt-5 mb-3">


                            <div class="col-md-2 mb-3">
                                <h6 class="text-uppercase font-weight-bold">
                                    <Link to="/aboutus">About us</Link>
                                </h6>
                            </div>

                            <div class="col-md-2 mb-3">
                                <h6 class="text-uppercase font-weight-bold">
                                    <Link to="/">Blog</Link>
                                </h6>
                            </div>

                            {/*<div class="col-md-2 mb-3">*/}
                            {/*    <h6 class="text-uppercase font-weight-bold">*/}
                            {/*        <Link to="/developers">Developers</Link>*/}
                            {/*    </h6>*/}
                            {/*</div>*/}

                            <div class="col-md-2 mb-3">
                                <h6 class="text-uppercase font-weight-bold">
                                    <Link to="/help">Help</Link>
                                </h6>
                            </div>

                            <div class="col-md-2 mb-3">
                                <h6 class="text-uppercase font-weight-bold">
                                    <a href="" data-toggle="modal"
                                       data-target="#modalContactForm">Contact</a>
                                </h6>
                            </div>


                        </div>

                        <hr class="rgba-white-light" className="hr1"/>



                            <div class="row d-flex text-center justify-content-center mb-md-0 mb-4">


                                <div class="col-md-8 col-12">
                                    <p className="paraLineHeight">Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                        accusantium doloremque laudantium, totam rem
                                        aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                                        explicabo.
                                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur.</p>
                                </div>


                            </div>

                            <hr class="clearfix d-md-none rgba-white-light" className="hr2"/>


                                <div class="row pb-3">


                                    <div class="col-md-12">

                                        <div class="mb-5 flex-center">


                                            <a class="fb-ic">
                                                <i class="fab fa-facebook-f fa-lg white-text mr-4"> </i>
                                            </a>

                                            <a class="tw-ic">
                                                <i class="fab fa-twitter fa-lg white-text mr-4"> </i>
                                            </a>

                                            <a class="gplus-ic">
                                                <i class="fab fa-google-plus-g fa-lg white-text mr-4"> </i>
                                            </a>

                                            <a class="li-ic">
                                                <i class="fab fa-linkedin-in fa-lg white-text mr-4"> </i>
                                            </a>

                                            <a class="ins-ic">
                                                <i class="fab fa-instagram fa-lg white-text mr-4"> </i>
                                            </a>

                                            <a class="pin-ic">
                                                <i class="fab fa-pinterest fa-lg white-text"> </i>
                                            </a>

                                        </div>

                                    </div>


                                </div>
                    </div>



                    <div class="footer-copyright text-center py-3">© 2019 Copyright:
                        <Link to="/">SeafaringWorld.com</Link>
                    </div>


                </footer>


                {/*<footer class="page-footer font-small mdb-color lighten-3 pt-4">*/}


                {/*    <div class="container text-center text-md-left">*/}


                {/*        <div class="row">*/}


                {/*            <div class="col-md-4 col-lg-3 mr-auto my-md-4 my-0 mt-4 mb-1">*/}


                {/*                <h5 class="font-weight-bold text-uppercase mb-4">Footer Content</h5>*/}
                {/*                <p>Here you can use rows and columns to organize your footer content.</p>*/}
                {/*                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit amet numquam iure provident voluptate*/}
                {/*                    esse*/}
                {/*                    quasi, veritatis totam voluptas nostrum.</p>*/}

                {/*            </div>*/}


                {/*            <hr class="clearfix w-100 d-md-none"/>*/}


                {/*                <div class="col-md-2 col-lg-2 mx-auto my-md-4 my-0 mt-4 mb-1">*/}


                {/*                    <h5 class="font-weight-bold text-uppercase mb-4">About</h5>*/}

                {/*                    <ul class="list-unstyled">*/}
                {/*                        <li>*/}
                {/*                            <p>*/}
                {/*                                <a href="#!">PROJECTS</a>*/}
                {/*                            </p>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <p>*/}
                {/*                                <a href="#!">ABOUT US</a>*/}
                {/*                            </p>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <p>*/}
                {/*                                <a href="#!">BLOG</a>*/}
                {/*                            </p>*/}
                {/*                        </li>*/}
                {/*                        <li>*/}
                {/*                            <p>*/}
                {/*                                <a href="#!">AWARDS</a>*/}
                {/*                            </p>*/}
                {/*                        </li>*/}
                {/*                    </ul>*/}

                {/*                </div>*/}


                {/*                <hr class="clearfix w-100 d-md-none"/>*/}


                {/*                    <div class="col-md-4 col-lg-3 mx-auto my-md-4 my-0 mt-4 mb-1">*/}


                {/*                        <h5 class="font-weight-bold text-uppercase mb-4">Address</h5>*/}

                {/*                        <ul class="list-unstyled">*/}
                {/*                            <li>*/}
                {/*                                <p>*/}
                {/*                                    <i class="fas fa-home mr-3"></i> New York, NY 10012, US</p>*/}
                {/*                            </li>*/}
                {/*                            <li>*/}
                {/*                                <p>*/}
                {/*                                    <i class="fas fa-envelope mr-3"></i> info@example.com</p>*/}
                {/*                            </li>*/}
                {/*                            <li>*/}
                {/*                                <p>*/}
                {/*                                    <i class="fas fa-phone mr-3"></i> + 01 234 567 88</p>*/}
                {/*                            </li>*/}
                {/*                            <li>*/}
                {/*                                <p>*/}
                {/*                                    <i class="fas fa-print mr-3"></i> + 01 234 567 89</p>*/}
                {/*                            </li>*/}
                {/*                        </ul>*/}

                {/*                    </div>*/}


                {/*                    <hr class="clearfix w-100 d-md-none"/>*/}


                {/*                        <div class="col-md-2 col-lg-2 text-center mx-auto my-4">*/}


                {/*                            <h5 class="font-weight-bold text-uppercase mb-4">Follow Us</h5>*/}


                {/*                            <a type="button" class="btn-floating btn-fb">*/}
                {/*                                <i class="fab fa-facebook-f"></i>*/}
                {/*                            </a>*/}

                {/*                            <a type="button" class="btn-floating btn-tw">*/}
                {/*                                <i class="fab fa-twitter"></i>*/}
                {/*                            </a>*/}

                {/*                            <a type="button" class="btn-floating btn-gplus">*/}
                {/*                                <i class="fab fa-google-plus-g"></i>*/}
                {/*                            </a>*/}

                {/*                            <a type="button" class="btn-floating btn-dribbble">*/}
                {/*                                <i class="fab fa-dribbble"></i>*/}
                {/*                            </a>*/}

                {/*                        </div>*/}


                {/*        </div>*/}


                {/*    </div>*/}



                {/*    <div class="footer-copyright text-center py-3">© 2018 Copyright:*/}
                {/*        <a href="https://mdbootstrap.com/education/bootstrap/"> MDBootstrap.com</a>*/}
                {/*    </div>*/}


                {/*</footer>*/}


            </div>
        );
    }
}

export default Footer;