import React, {Component} from 'react';
import axios from 'axios';
import './EditBlog.css';
import Navbar from "../Navigation/Navbar";

class EditBlog extends Component {

    state={
        name:'',
        image:'',
        description:'',
        loading:true
    };

    componentDidMount() {

        window.scrollTo(0,0);

        axios.get("https://peaceful-wind-cave-44493.herokuapp.com/campgrounds/"+this.props.match.params._id+"/edit")
            .then(response=>{
                if(response.data!==''){
                    this.setState({name:response.data.name,image:response.data.image,description:response.data.description,loading:false})
                }
            }).catch(err=>{
                console.log("error");
                console.log(err);
        })
    }

    submitEditBlog = (e) =>{
        e.preventDefault();

        let blog={name:this.state.name,image:this.state.image,description:this.state.description};
        console.log({blog});
        axios.put("https://peaceful-wind-cave-44493.herokuapp.com/campgrounds/"+this.props.match.params._id,{blog})
            .then(response=>{
                console.log(response);
                if(response.data.editMessage==='success'){
                    window.location="/bloglist/details/"+this.props.match.params._id;
                }
            }).catch(err=>{
            console.log(err);
        })
    };

    render() {

        let username = window.localStorage.getItem("username");

        let loader = (
            <div className="d-flex justify-content-center">
                <div className="spinner-grow text-primary" style={{width: "20rem", height: "20rem" ,marginTop:"11em"}} role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        );

        //if(!this.state.loading && username==='seafaring'){
        if(!this.state.loading){
            loader=(
                <div className="ui main text container segment ownContainer">
                    <div className="ui huge header">Edit - {this.state.name}</div>
                    <form className="ui form">
                        <div className="field">
                            <label>Title</label>
                            <input type="text" name="blog[title]" value={this.state.name} onChange={(event) => this.setState({name: event.target.value})}/>
                        </div>
                        <div className="field">
                            <label>Title</label>
                            <input type="text" name="blog[image]" value={this.state.image} onChange={(event) => this.setState({image: event.target.value})}/>
                        </div>
                        <div className="field">
                            <label>Title</label>
                            <textarea name="blog[body]" value={this.state.description} onChange={(event) => this.setState({description: event.target.value})}></textarea>
                        </div>
                    </form>
                    <div className="ownButton">
                        <button className="ui violet inverted big button" onClick={this.submitEditBlog}>SUBMIT</button>
                    </div>
                </div>
            )
        }

        return (
            <div>
                <Navbar/>
                <div className='mySection'>
                    {loader}
                </div>
            </div>
        );
    }
}

export default EditBlog;