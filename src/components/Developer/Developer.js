import React, {Component} from 'react';

class Developer extends Component {
    componentDidMount() {
        window.scrollTo(0,0);
    }

    render() {
        console.log(this.props);
        return (
            <div className="container">

                <section class="team-section text-center my-5 mySection">


                    <h2 class="h1-responsive font-weight-bold my-5">Our amazing team</h2>

                    <p class="grey-text w-responsive mx-auto mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        Fugit, error amet numquam iure provident voluptate esse quasi, veritatis totam voluptas nostrum quisquam
                        eum porro a pariatur veniam.</p>


                    <div class="row" style={{justifyContent:"center",paddingBottom:'20px'}}>


                        <div class="col-lg-3 col-md-6 mb-lg-0 mb-5" style={{margin:"80px"}}>
                            <div class="avatar mx-auto">
                                <img src="https://lh3.googleusercontent.com/yDrB9gs1NVoFPNACBOmdxjaWQk1Pfc-iNbIb2G0mtNmR_pP3RY6NAPm8zQsDLLMPQCdFH4n5pOc6uR0m2bTQ7nzejfjzvAy3YaKNm_rjC6XPCtgxB_yZCSCmW4Dt0dkjUWFkyNeN8ewjlwIFAptp4nQ8HQyR8zBXfw4ZWGTmyHS1OSJJO5zWXxbxda4PcX1XwCRDRz36PqAWIsPJpYMDbSGEOgnp-GHwFX6it0EI7iengmnT1ioK8K1UYfW13wCLijq7PnpsTpPuGFiOBbdXqSATqojDHJoKruCtlb32buMVTyMJ-646HumxOmRer-r40F1OSVPbEoj5qfGA_iKzaEoBFFvFLWxByL2yNpWq-iuW8RGYNveDoG5r6ACms8HCMOPuKWuNAkcVMkcT0ihu6o_9xEMM8kWsF-1UzqutZqGYi2qIqMuNSQd4ScjQ7d_eVQonVJM4mnoWv0QX3tTeCI3KN2n3uXcRFTnEB_9oKpQa91EpxCg7D-_qbRvEqLhkJKAhUIHxK9PQxTfyxf4Qv9e6qRmKSLuoqAPxTMW0-d6BiuHO5Igdu-kxCGKSKOcancXNVsqhisjVqqv9oQmCX89clCz0xYKPYMgy9IXUZB6POdHQ620DVsPcLikq7rktGTtBhp0_tOkPotTc383MhVCnfa9JHwEU4muvV_0ZGsi7vez2GCSuRsTzSq8IajDCaJBlLKyt2BxOMzaFefLEhGwq4g=w576-h625-no" class="rounded-circle z-depth-1"
                                      style={{height:'18em',width:'19em'}} alt="ankit saxena"/>
                            </div>
                            <h5 class="font-weight-bold mt-4 mb-3">Ankit Saxena</h5>
                            <p class="text-uppercase blue-text"><strong>Full-Stack Web Developer</strong></p>
                            <p class="grey-text">Hey there I am FrontEnd Developer</p>
                            <p style={{color:"red"}}><b>Email : </b>ankit.saxena@seafaringworld.com</p>
                            <p className='black-text'>Click one of the links below to contact Me</p>
                            <ul class="list-unstyled mb-0">

                                <a class="p-2 fa-lg fb-ic" href="https://www.facebook.com/ankit.saxena.7146">
                                    <i class="fab fa-facebook-f blue-text"> </i>
                                </a>

                                <a class="p-2 fa-lg tw-ic" href="https://www.instagram.com/deadlock.ankit/">
                                    <i class="fab fa-instagram blue-text"> </i>
                                </a>
                            </ul>
                        </div>

                        <div class="col-lg-3 col-md-6 mb-lg-0 mb-5" style={{margin:"80px"}}>
                            <div class="avatar mx-auto">
                                <img src="https://lh3.googleusercontent.com/NvWlt9DGf4kcQcSAjLEYCXwm9PO5OOvhplmosZHG2ETrG-uARN6A78Ii2NuRwKQnLliW4q_8Pocqedsa4TrDe7DR72tfkYHmtWT72aifTxhMJzNJrDQd9nZn53SpKKdHQOt6qGNQ3WuzIcXCCOgclPmXg3J73G0h-3cNvw4Uj-nm0FKAnT6P9IFcCa3rFVPjcJg6Gh2FmZzi8usW_OV2OOelqiTGRHXI_suhqB20tz3BB82wfFRIcFMGvWIjQPz2vlJaaglr_8JRWG2RJ7E6BwgwVsIIz-xQRA9dxvduKD7mJl086_A48lwtSBlZCmDXb8RoCmkzZRVG-lbQ6MgmhXwwn-NWVaj4FQ5IPtPVCNb162ruIBFASmFhFJ3PZPmK_PptCgsk4UkRaE-id-g5LoLE9N_2PyYEV171Q9It47ObVYxb8MaJOrdTU9K5R1VQ5KCGPunkz31cXyemRHwjQXpCSJMFbST_4AhuYQwMGMWKTsNrp8waTnJZdDXVL0ukk6NnUgrDoiHO0jIpoGawAyooj6f-eq8yXCBXTq85Ib0wUFLdSP7k5sFiO2xAf1DgDm1wfbBwK1R4E9b82130pAt46KRGQsqrHHE4gN0QO1PFUHAAdOc8KkmS5856JBD9TyFmDjryF-FOcLzKo0Vx-orYURxNp2QLKqAkfDPObrp3SgWh7beIpehw3I4mk4q_oA4iOlBWnkSLytPiky92SEJhxA=w461-h625-no" class="rounded-circle z-depth-1"
                                     style={{height:'18em',width:'19em'}} alt="Sample avatar"/>
                            </div>
                            <h5 class="font-weight-bold mt-4 mb-3">Swapnil Dwivedi</h5>
                            <p class="text-uppercase blue-text"><strong>Full-Stack Web Developer</strong></p>
                            <p className="grey-text">Hey there I am BackEnd Developer</p>
                            <p style={{color:"red",fontSize:'0.9rem'}}><b>Email : </b>swapnil.dwivedi@seafaringworld.com</p>
                            <p className='black-text'>Click one of the links below to contact Me</p>
                            <ul class="list-unstyled mb-0">

                                <a class="p-2 fa-lg fb-ic" href="https://www.facebook.com/swapnil.dwivedi.94">
                                    <i class="fab fa-facebook-f blue-text"> </i>
                                </a>

                                <a class="p-2 fa-lg ins-ic" href="https://www.instagram.com/dwivedi.swapnil/">
                                    <i class="fab fa-instagram blue-text"> </i>
                                </a>
                            </ul>
                        </div>

                    </div>


                </section>


            </div>
        );
    }
}

export default Developer;