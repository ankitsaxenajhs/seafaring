import React from 'react';
import logo from './logo.svg';
import {Switch,Route} from 'react-router-dom';
import BlogList from './components/BlogList/BlogList';
import Landing from './components/Landing/Landing';
import BlogDetail from './components/BlogDetail/BlogDetail';
import NewBlog from "./components/NewBlog/NewBlog";
import Navbar from "./components/Navigation/Navbar";
import EditBlog from "./components/EditBlog/EditBlog";
import Footer from './components/Footer/Footer';
import Developer from './components/Developer/Developer';

function App() {
  return (
    <div>
      <Navbar/>
      <Switch>
        <Route path="/" exact component={Landing}/>
        <Route path="/bloglist" exact component={BlogList}/>
        <Route path="/bloglist/details/:_id" exact component={BlogDetail}/>
        <Route path="/bloglist/new" exact component={NewBlog}/>
        <Route path="/bloglist/details/:_id/edit" exact component={EditBlog}/>
        <Route path="/developers" exact component={Developer}/>
      </Switch>
      <Footer/>
    </div>
  );
}

export default App;
